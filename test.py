# Demo file for Spyder Tutorial
# Hans Fangohr, University of Southampton, UK

from datetime import datetime, timedelta
import time
from collections import namedtuple
import pandas as pd
import requests
import matplotlib.pyplot as plt
from plotrelationships import plot_relationships, plot_outliers

#https://api.weather.com/v2/pws/history/hourly?stationId=ISYDNEY529&format=json&units=m&date=20181001&apiKey=06e69919b82142bfa69919b821f2bf8e
#BASE_URL = "http://api.wunderground.com/api/{}/history_{}/q/AU/Sydney.json"

API_KEY = '06e69919b82142bfa69919b821f2bf8e'
STATION_ID = 'ISYDNEY529'
BASE_URL = "https://api.weather.com/v2/pws/history/hourly?stationId={}&format=json&units=m&date={}&apiKey={}"

target_date = datetime(2018, 10, 3)#YYYYMMDD

features = ["date", "tempAvg", "dewptAvg", "pressureTrend", "humidityHigh", "humidityLow", "tempHigh",
            "tempLow", "dewptHigh", "dewptLow", "pressureMax", "pressureMin", "precipTotal"]

HourlySummary = namedtuple("HourlyReport", features)


def extract_weather_data(url, station_id, api_key, target_date, days):
    DailySummary = {}
    records = {}
    df_records = []
    for _ in range(days):
        request = url.format(station_id, target_date.strftime('%Y%m%d'), api_key)
        print(request)
        response = requests.get(request)
        if response.status_code == 200:
            for i in range(len(response.json()['observations'])):
                data = response.json()['observations'][i]
                print(data)
                DailySummary[i]=HourlySummary(
                    #date=target_date,
                    date=data['obsTimeLocal'],
                    tempAvg=data['metric']['tempAvg'],
                    dewptAvg=data['metric']['dewptAvg'],
                    pressureTrend=data['metric']['pressureTrend'],
                    humidityHigh=data['humidityHigh'],
                    humidityLow=data['humidityLow'],
                    tempHigh=data['metric']['tempHigh'],
                    tempLow=data['metric']['tempLow'],
                    dewptHigh=data['metric']['dewptHigh'],
                    dewptLow=data['metric']['dewptLow'],
                    pressureMax=data['metric']['pressureMax'],
                    pressureMin=data['metric']['pressureMin'],
                    precipTotal=data['metric']['precipTotal'])
        time.sleep(6)
        df_records.append(DailySummary[0])
        records[target_date]= DailySummary
        target_date += timedelta(days=1)
    return records, df_records


def derive_nth_day_feature(df, feature, N):
    rows = df.shape[0]
    nth_prior_measurements = [None]*N + [df[feature][i-N] for i in range(N, rows)]
    col_name = "{}_{}".format(feature, N)
    df[col_name] = nth_prior_measurements
    

##################################################
### MAIN FUNCTION STARTS HERE ##
##################################################
    
##################################################
[records,df_records] = extract_weather_data(BASE_URL, STATION_ID, API_KEY, target_date, 3)
############## ^^^^ GET RECORDS ^^^^#########################

df = pd.DataFrame(df_records, columns=features).set_index('date')
df.columns
for feature in features:
    if feature != 'date':
        for N in range(1,4):
            derive_nth_day_feature(df,feature, N)
            
#before removing anything
df.columns
# make list of original features without tempAvg, tempLow, and tempHigh
to_remove = [feature 
             for feature in features 
             if feature not in ['tempAvg', 'tempLow', 'tempHigh']]

# make a list of columns to keep
to_keep = [col for col in df.columns if col not in to_remove]

# select only the columns in to_keep and assign to df
df = df[to_keep]
#after removing
df.columns
#before converting
df.info()
df = df.apply(pd.to_numeric, errors='coerce')
#after converting
df.info()


##################################################
###FILL GAPS IN DATA###
##################################################
for precip_col in ['precipTotal_1', 'precipTotal_2', 'precipTotal_3']:
    # create a boolean array of values representing nans
    missing_vals = pd.isnull(df[precip_col])
    df[precip_col][missing_vals] = 0

df = df.dropna()
##################################################
###END FILL GAPS IN DATA###
##################################################

plot_outliers(df.tempHigh_1, 'tempHigh', df=df)

##################################################
###BEGIN CORRELATION ANALYSIS###
##################################################
df.corr()[['tempAvg']].sort_values('tempAvg') ##This outputs correlations of all columns in df with tempAvg value, so
####################################################Choosing the ones above roughly 0.6 as predictors of tempAvg
predictors = ['tempAvg_1',  'tempAvg_2',  'tempAvg_3', 
          'tempLow_1',   'tempLow_2',   'tempLow_3',
          'dewptAvg_1', 'dewptAvg_2', 'dewptAvg_3',
          'dewptHigh_1',  'dewptHigh_2',  'dewptHigh_3',
          'dewptLow_1',  'dewptLow_2',  'dewptLow_3',
          'tempHigh_1',   'tempHigh_2',   'tempHigh_3']
df2 = df[['tempAvg'] + predictors]
    
plot_relationships(predictors,df2)
##################################################
###END CORRELATION ANALYSIS###
##################################################
